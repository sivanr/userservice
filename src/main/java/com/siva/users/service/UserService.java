package com.siva.users.service;

import com.siva.users.model.User;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

public interface UserService {
     Single<Integer> save(User user);

    Flowable<User> findAll();

    Maybe<User> findById(Integer id);
     Maybe<User> findByLogin(String login);
     Completable delete(Integer id);
     Maybe<User> update(Integer id, User user);
}
