package com.siva.users.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "User", description = "User Details")
public class User {
    private Integer id;
    @Schema(description = "Login name", maxLength = 8)
    private String login;
    @Schema(description = "User full name", maxLength = 128)
    private String fullName;
    @Schema(description = "Login password", minLength = 6)
    @Min(value = 6, message = "Password requires at least 6 characters")
    private String password;
    @Schema(description = "User email", maxLength = 256)
    @Email(message = "Not a valid email")
    private String email;
    private LocalDateTime updatedTime;
}
