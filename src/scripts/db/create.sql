-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id serial,
    login character varying(8)  NOT NULL,
    full_name character varying(128)  NOT NULL,
    password character varying(256) ,
    email character varying(256) ,
    updated_time timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT users_login_uq UNIQUE (login)

)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.users
    OWNER to postgres;