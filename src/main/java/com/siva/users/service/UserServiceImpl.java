package com.siva.users.service;

import com.siva.users.model.User;
import com.siva.users.repository.UserRepository;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Inject
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Single<Integer> save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Flowable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Maybe<User> findById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public Maybe<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public Completable delete(Integer id) {
        return userRepository.delete(id);
    }

    @Override
    public Maybe<User> update(Integer id, User user) {
        user.setId(id);
        Maybe<User> userMaybe = findById(id).cache();
        return userMaybe.isEmpty().flatMapMaybe(res -> {
           if(res) {
               return Maybe.empty();
           } else {
               return userRepository.update(user);
           }
        });
    }
}
