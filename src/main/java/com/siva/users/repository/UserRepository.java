package com.siva.users.repository;

import com.siva.users.model.User;
import io.reactiverse.reactivex.pgclient.*;
import io.reactivex.*;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.concurrent.Flow;

@Singleton
public class UserRepository {

    private static final String INSERT_SQL = "INSERT INTO users (login, full_name, password, email) VALUES ($1,$2,$3,$4) RETURNING id";
    private static final String UPDATE_SQL = "UPDATE users SET full_name = $1, email = $2, updated_time = $3 WHERE id = $4";
    private static final String SELECT_ALL = "SELECT * FROM users";
    private static final String SELECT_BY_ID = "SELECT * FROM users WHERE id = $1";
    private static final String DELETE_BY_ID = "DELETE FROM users WHERE id = $1";
    private static final String SELECT_BY_LOGIN = "SELECT * FROM users WHERE login = $1";

    private PgPool pgPool;

    @Inject
    public UserRepository(PgPool pgPool) {
        this.pgPool = pgPool;
    }

    public Single<Integer> save(User user) {
        Tuple data = Tuple.of(user.getLogin(), user.getFullName(), user.getPassword(), user.getEmail());
        return pgPool.rxBegin()
                .flatMap(tx -> tx.rxPrepare(INSERT_SQL)
                        .flatMap(pgPreparedQuery -> pgPreparedQuery.rxExecute(data))
                        .map(this::getId)
                        .doOnSuccess(res -> tx.commit())
                        .doOnError(throwable -> tx.rollback())
                );
    }

    public Flowable<User> findAll() {
        return pgPool.rxBegin().flatMapPublisher(tx ->
                tx.rxPrepare(SELECT_ALL)
                        .flatMapPublisher(pgPreparedQuery ->
                                pgPreparedQuery.createStream(50, Tuple.tuple()).toFlowable()
                        .flatMap(this::getUser)
                        ).doAfterTerminate(tx::commit)
        );
    }

    public Maybe<User> findById(Integer id) {
        return pgPool.rxPreparedQuery(SELECT_BY_ID, Tuple.of(id))
                .flatMapMaybe(this::getUser);
    }

    public Maybe<User> findByLogin(String login) {
        return pgPool.rxPreparedQuery(SELECT_BY_LOGIN, Tuple.of(login))
                .flatMapMaybe(this::getUser);
    }

    public Completable delete(Integer id) {
        return pgPool.rxBegin()
                .flatMapCompletable(tx -> tx.rxPreparedQuery(DELETE_BY_ID, Tuple.of(id))
                        .flatMapCompletable(pgRowSet -> Completable.complete())
                        .doOnComplete(tx::commit)
                        .doOnError(th -> tx.rollback())
                );
    }

    public Maybe<User> update(User user) {
        Tuple data = Tuple.of(user.getFullName(), user.getEmail(), LocalDateTime.now(), user.getId());
        return pgPool.rxBegin()
                .flatMapMaybe(tx -> tx.rxPrepare(UPDATE_SQL)
                        .flatMap(pgPreparedQuery -> pgPreparedQuery.rxExecute(data))
                        .flatMapMaybe(res -> Maybe.just(user))
                        .doOnSuccess(res -> tx.commit())
                        .doOnError(throwable -> tx.rollback())
                );
    }

    private Integer getId(PgRowSet pgRowSet) {
        Integer id = null;
        PgIterator iterator = pgRowSet.iterator();
        while (iterator.hasNext()) {
            Row row = iterator.next();
            id = row.getInteger("id");
        }
        return id;
    }

    private Maybe<User> getUser(PgRowSet pgRowSet) {
        User user = null;
        PgIterator iterator = pgRowSet.iterator();
        while (iterator.hasNext()) {
            Row row = iterator.next();
            user = new User();
            user.setId(row.getInteger("id"));
            user.setLogin(row.getString("login"));
            user.setFullName(row.getString("full_name"));
            user.setEmail(row.getString("email"));
        }
        return user == null ? Maybe.empty() : Maybe.just(user);
    }

    private Flowable<User> getUser(Row row) {
        User user = new User();
        user.setId(row.getInteger("id"));
        user.setLogin(row.getString("login"));
        user.setFullName(row.getString("full_name"));
        user.setEmail(row.getString("email"));
        return Flowable.just(user);
    }
}
