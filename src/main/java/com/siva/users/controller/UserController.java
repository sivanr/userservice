package com.siva.users.controller;

import com.siva.users.model.User;
import com.siva.users.service.UserService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.net.URI;

@Controller("/users")
public class UserController {

    private UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * @param user User details
     * @return Http Response with status 201 and location header with created resource link
     */
    @Operation(summary = "Creates a User", description = "Creates a new User resource in the system")
    @ApiResponse(responseCode = "201", description = "User created successfully")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @Post(consumes = MediaType.APPLICATION_JSON)
    public Single<HttpResponse> save(
        @RequestBody(description = "User details json", required = true,
            content = @Content( mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = User.class)
            )) @Body @NotNull Single<User> user) {
        return user.flatMap(userService::save).map(id -> HttpResponse.created(URI.create("/users/" + id)));
    }


    /**
     * Returns user details
     * @param id user id
     * @return user details
     */
    @Operation(summary = "Get user details")
    @ApiResponse(responseCode = "200", description = "User is retrieved successfully")
    @ApiResponse(responseCode = "404", description = "User not found in the system")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @Get("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Maybe<User> findByid(@PathVariable @Positive Integer id) {
        return userService.findById(id);
    }

    /**
     * Returns all users
     * @return user list
     */
    @Operation(summary = "Get user list")
    @ApiResponse(responseCode = "200", description = "Users are retrieved successfully")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public Flowable<User> findAll() {
        return userService.findAll();
    }

    /**
     * Updates user
     * @param id User id
     * @param user User details
     * @return Http Response with status 200 if successful
     */
    @Operation(summary = "Updates a User", description = "Updates a User resource in the system")
    @ApiResponse(responseCode = "200", description = "User updated successfully")
    @ApiResponse(responseCode = "404", description = "User not found")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @Put(value = "/{id}", consumes = MediaType.APPLICATION_JSON)
    public Maybe<User> update(@PathVariable Integer id,
            @RequestBody(description = "User details json", required = true,
                    content = @Content( mediaType = MediaType.APPLICATION_JSON,
                            schema = @Schema(implementation = User.class)
                    )) @Body Single<User> user) {
        return user.flatMapMaybe(u -> userService.update(id, u));
    }

    /**
     * Deletes user from the system
     * @param id user id
     * @return Http Response with status code
     */
    @Operation(summary = "Deletes user")
    @ApiResponse(responseCode = "201", description = "User is deleted successfully if present")
    @ApiResponse(responseCode = "500", description = "Internal server error")
    @Delete("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Single<HttpResponse> delete(@PathVariable @Positive Integer id) {
        return userService.delete(id).toSingle(HttpResponse::accepted);
    }
}
